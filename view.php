<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <?php 
        $connect = new mysqli('localhost','root','','phpb3');
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            $qry = "SELECT * FROM users WHERE id = $id";
            $result = $connect->query($qry);
            $data = $result->fetch_assoc();
        }
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <ul class="list-group">
                    <li class="list-group-item">
                        <strong>Name</strong><br/>
                        <?php echo $data['username']; ?>
                    </li>
                    <li class="list-group-item">
                        <strong>Email</strong><br/>
                        <?php echo $data['email']; ?>
                    </li>
                </ul>
                <a href="index.php" class="btn btn-success">Goto Home Page</a>
            </div>
        </div>
    </div>
</body>
</html>